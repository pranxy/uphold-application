import HtmlWebpackPlugin from 'html-webpack-plugin';
import path from 'path';
import webpack from 'webpack';

export default {
  devServer: {
    contentBase: path.resolve(process.cwd(), 'public'),
    proxy: {
      '**': {
        target: 'https://api.uphold.com/v0',
        changeOrigin: true,
      },
    },
  },
  entry: path.resolve(process.cwd(), 'index.js'),
  module: {
    rules: [{
      exclude: /node_modules/,
      loader: 'babel-loader',
      test: /\.js?$/,
    }],
  },
  mode: 'development',
  output: {
    filename: '[name].[hash].js',
    path: path.resolve(process.cwd(), 'public'),
    publicPath: '/',
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(process.cwd(), 'index.html'),
    }),
  ],
};
