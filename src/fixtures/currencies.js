const Currencies = [
  {
    id: 0,
    currency: 'USD',
  },
  {
    id: 1,
    currency: 'EUR',
  },
  {
    id: 2,
    currency: 'GBP',
  },
];

export { Currencies as default };
