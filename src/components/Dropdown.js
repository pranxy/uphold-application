import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Dropdown extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(e.target.value);
    }
  }

  render() {
    return (
      <div>
        <span>Change currency:</span>
        <select
          id="select1"
          value={this.props.selectedCurrency.key}
          onChange={this.handleChange}
          disabled={this.props.isLoading}
        >
          {this.props.currencies.map(item => (
            <option key={item.id} value={item.currency}>{item.currency}</option>
          ))}
        </select>
      </div>
    );
  }
}

Dropdown.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  selectedCurrency: PropTypes.shape({
    key: PropTypes.string,
    amount: PropTypes.any,
  }).isRequired,
  onChange: PropTypes.func.isRequired,
  currencies: PropTypes.arrayOf(PropTypes.any).isRequired,
};

export default Dropdown;
