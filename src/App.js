import React, { Component } from 'react';
import Dropdown from './components/Dropdown';

class App extends Component {
  constructor() {
    super();
    this.state = {
      isLoading: true,
      list: [],
      selectedCurrency: {
        key: 'USD',
        amount: 0,
      },
    };

    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    fetch('/reserve/statistics', {
      method: 'GET',
    })
      .then(result => result.json()) // still returns a promise object, U need to chain it again
      .then((currencies) => {
        const list = currencies.map((item, index) => ({
          id: index,
          currency: item.currency,
        }));

        this.setState({ list });
        localStorage.setItem('currencies', JSON.stringify(currencies));
        this.setSelectedCurrency('USD');
        this.setState({ isLoading: false });
      });
  }

  setSelectedCurrency(key) {
    const storage = JSON.parse(localStorage.getItem('currencies'));

    const coin = storage.find(item => item.currency === key);

    const selectedCurrency = Object.assign({}, this.state.selectedCurrency);
    selectedCurrency.key = coin.currency;
    selectedCurrency.amount = coin.totals.amount;
    this.setState({ selectedCurrency });
    this.setState({ isLoading: false });
  }

  // Event when currency is changed
  handleChange(key) {
    this.setState({ isLoading: true });
    this.setSelectedCurrency(key);
    this.updateCurrencies();
  }

  updateCurrencies() {
    fetch('/reserve/statistics', {
      method: 'GET',
    })
      .then(result => result.json()) // still returns a promise object, U need to chain it again
      .then((currencies) => {
        localStorage.setItem('currencies', JSON.stringify(currencies));

        this.setSelectedCurrency(this.state.selectedCurrency.key);
      });
  }

  render() {
    return (
      <div style={{
 fontFamily: 'sans-serif', margin: '0 auto', width: '100%', textAlign: 'center',
}}
      >
        <h1>Uphold&apos;s Reserve Status</h1>

        <h3 className="amount">{this.state.selectedCurrency.amount}
          <span className="amount-currency" style={{ marginLeft: '5px' }}>{this.state.selectedCurrency.key}</span>
        </h3>

        <Dropdown
          isLoading={this.state.isLoading}
          currencies={this.state.list}
          selectedCurrency={this.state.selectedCurrency}
          onChange={this.handleChange}
        />
      </div>
    );
  }
}
export default App;
