import React from 'react';
import { shallow } from 'enzyme';

import Dropdown from '../components/Dropdown';
import Currencies from '../fixtures/currencies';

describe('<Dropdown />', () => {
  const selected = {
    key: 'USD',
    amount: 0,
  };

  const DROPDOWN_PROPS = {
    currencies: Currencies,
    onChange: jest.fn(),
    selectedCurrency: selected,
    isLoading: false,
  };

  const wrapper = shallow(<Dropdown {...DROPDOWN_PROPS} />);

  afterEach(() => {
    DROPDOWN_PROPS.onChange.mockClear();
  });

  it('should contain a list of currencies', () => {
    const listWrapper = wrapper.find('option');
    expect(listWrapper.length).toBe(3);
  });

  it('should trigger change event', () => {
    wrapper.find('select').simulate('change', { target: { value: '1' } });

    expect(DROPDOWN_PROPS.onChange).toHaveBeenCalled();
  });
});
