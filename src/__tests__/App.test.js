import React from 'react';
import { shallow } from 'enzyme';

import App from '../App';

import Statistics from '../fixtures/statistics';

describe('App', () => {
  let component;

  beforeEach(() => {
    global.fetch = jest.fn().mockResolvedValue({
      json: () => Statistics,
    });
    component = shallow(<App />);
  });

  afterEach(() => {
    component = undefined;
  });

  it('fetch is called', () => {
    expect(global.fetch).toHaveBeenCalled();
  });

  it('should GET the /reserve/statistics', () => {
    expect(global.fetch).toHaveBeenCalledWith('/reserve/statistics', { method: 'GET' });
  });

  it('fetch received data', () => {
    expect(component.state().list.length).toBeGreaterThan(1);
  });

  it('should save to localStorage', () => {
    const KEY = 'currencies';
    const VALUE = JSON.stringify(Statistics);

    expect(localStorage.setItem).toHaveBeenLastCalledWith(KEY, VALUE);
    expect(localStorage.__STORE__[KEY]).toBe(VALUE);
    expect(Object.keys(localStorage.__STORE__).length).toBe(1);
  });

  it('should have title', () => {
    expect(component.find('h1').length).toBe(1);
    expect(component.find('h1').at(0).text()).toBe("Uphold's Reserve Status");
  });

  it('renders Dropdown component', () => {
    expect(component.find('Dropdown').exists()).toBe(true);
  });
});
