describe('Uphold application', () => {
  beforeEach(() => {
    cy.visit('');
  });

  it('should have the title "Uphold\'s Application Challenge"', () => {
    cy.title().should('eq', "Uphold's Application Challenge");
  });

  describe('query DOM elements', () => {
    it('should have a heading with "Uphold\'s Reserve Status"', () => {
      cy.get('h1').contains("Uphold's Reserve Status");
    });

    it('subtitle should have default currency', () => {
      cy.get('.amount-currency').contains('USD');
    });

    it('should have select element with default currency', () => {
      cy.get('#select1').should('contain', 'USD');
    });
  });

  describe('Dropdown', () => {
    it('should select an option from select', () => {
      cy.get('#select1').select('ETH');

      cy.get('#select1').should('contain', 'ETH');

      cy.get('.amount-currency').contains('ETH');
    });
  });
});
