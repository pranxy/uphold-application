# Uphold's Application Challenge

**As a** User **I want to** see the Uphold's reserve total amount in different currencies.

## Mockup

![uphold-application-challenge](uphold-application-challenge.png)

## Acceptance criteria

- **TC01**: Should see the total USD amount by default.
- **TC02**: Should be able to change between currencies.
- **TC03**: Should have all the values cached upon the first request.
- **TC04**: Should use the cached value when changing between currencies.
- **TC05**: Should make a new API call in background, re-populating the cached value, when changing between currencies.

## Technical specs

To solve this problem the developer:

- **MUST** use React.
- **MUST** use ES6.
- **SHOULD** have proper linting tools.
- **MAY** implement unit tests.
- **MAY** implement E2E tests.

**Note:** Only MUST points are mandatory.

Read more about the [Reserve Status](https://uphold.com/en/developer/api/documentation/#reserve-status) to achieve the proposed goal.

## Setup the project

1. Clone it:

  ```sh
  $ git clone git@github.com:uphold/uphold-application-challenge.git
  ```

2. Install dependencies:

  ```sh
  $ npm install
  ```

3. Run the project:

  ```
  $ npm start
  ```

  **Note:** The project will be running at [http://localhost:8080](http://localhost:8080).

#### Check the source code for linting errors
```
$ npm run lint
```

#### Fix linting errors
```
$ npm run lint:fix
```

#### Run the unit tests
```
$ npm run test
```

#### Run the e2e tests
```
$ npm run e2e
```

#### Run e2e in debug mode / GUI
```
$ npm run e2e:debug
```