const request = require('request');
const express = require('express');

const app = express();

app.use('/', express.static('public'));
app.use('/reserve/*', (req, res) =>
  request.get({ url: `https://api.uphold.com/v0${req.originalUrl}` }, (error, response, body) => res.send(body)));

app.listen(3000, () => console.log('Listening on port 3000'));
